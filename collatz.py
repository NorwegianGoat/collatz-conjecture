'''collatz.py
Copyright (C) 2019 <riccardomioli@gmail.com>
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

#A simple Python 3.7 program to test the Collatz conjecture

import sys as System
from datetime import datetime

collatzList=[]
def collatzAlg(n):
    file=open("CollatzConjecture.txt", "a+")
    if(n>0):
        if(n==1):
            print(collatzList)
            for i in range(len(collatzList)):
                file.write(str(collatzList[i])+" ")
            file.write("\n\n")
            collatzList.clear()
            file.close()
        elif(n%2==0):
            n//=2
            collatzList.append(n)
            collatzAlg(n)
        else:
            n=3*n+1
            collatzList.append(n)
            collatzAlg(n)

def main():
    while(True):
        try:
            sceltaUtente=int(input("Premere 0 per uscire, altrimenti inserire un altro \n"
            +"numero su cui calcolare la congettura di Collatz\n"))
            if(sceltaUtente>0):
                file=open("CollatzConjecture.txt", "a+")
                file.write("Data: " + str(datetime.now()) + " Numero: " + str(sceltaUtente) +" -> ")
                file.close()
                collatzAlg(sceltaUtente)
            else:
                System.exit(0)
        except ValueError:
            print("Inserire un valore numerico")

main()
