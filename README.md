# Collatz Conjecture

collatz.py is a program to test the Collatz Conjecture and get out from my Java's comfort zone.

## Running collatz.py

collatz.py contains all the necessary to "test" the Collatz Conjecture. In order to run the program
you must:

1. Open a ```new shell```

2. Set the ```working directory``` of the shell as the dir in which you downloaded the ```collatz.py```

3. Execute this command

```sh
python3 collatz.py
```

4. The program will ask you on what number you want to calculate the C. conjecture, then it will print a string
representing the Collatz succession

*N.B. The program will write a file named ```CollatzConjecture.txt```. In this file you can see the history of all the Collatz sequences you calculated*

5. When you have finished to calculate the various Collatz successions you can ```exit``` by pressing 0

## Authors

- **Riccardo Mioli** : [@NorwegianGoat] (https://gitlab.com/NorwegianGoat)


## License

This project is licensed under the GNU v.3 License - see the [LICENSE](LICENSE)
file for details
